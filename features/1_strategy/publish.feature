@notimplemented @strategy @strategy.publish
Feature: build_harness publishes artifacts
  When someone deploys *build_harness* into a CI pipeline they want to be able to
  publish Python packages to an identified repository such as PyPI.org or equivalent
  PEP-503 server.
