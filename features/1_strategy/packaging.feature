@notimplemented @strategy @strategy.packaging
Feature: build_harness packaged as a Python wheel
  When someone deploys *build_harness* into a CI pipeline they want to consume a
  wheel package for easy installation using conventional Python operations such as
  `pip`.
