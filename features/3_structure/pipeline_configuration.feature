@notimplemented @structure @structure.pipelines @relates_to.strategy.subcommands
Feature: Generate pipeline configuration file
  As a user I want build_harness to generate an initial pipeline configuration file for
  my pipeline application so that I can more quickly get started with a pipeline.

  Scenario: Generate a .gitlab-ci.yml file
  Scenario: Generate Github Actions Workflow file(s)
  Scenario: Generate Circle-CI file(s)
  Scenario: Generate Travis-CI file(s)
  Scenario: Generate Jenkins file(s)
