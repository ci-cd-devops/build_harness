#  Copyright (c) {{cookiecutter.creation_year}} {{cookiecutter.author}}
#
#  This file is part of {{cookiecutter.project_slug}}.

"""{{cookiecutter.project_summary}}"""

from ._version import __version__  # noqa: F401
